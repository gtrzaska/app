import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-main-screen-button',
  templateUrl: './main-screen-button.component.html',
  styleUrls: ['./main-screen-button.component.css']
})

export class MainScreenButtonComponent implements OnInit {
  @Input() text: string;
  @Input() imgSrc: string;
  @Input() isAddButton: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
