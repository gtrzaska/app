import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainScreenButtonComponent } from './main-screen-button.component';

describe('MainScreenButtonComponent', () => {
  let component: MainScreenButtonComponent;
  let fixture: ComponentFixture<MainScreenButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainScreenButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScreenButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
